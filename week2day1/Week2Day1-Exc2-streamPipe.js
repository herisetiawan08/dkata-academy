const fs = require('fs')
const { Transform } = require('stream')
reading = fs.createReadStream('./anotherFile.txt')
writing = fs.createWriteStream('./anotherFile-write.txt')
String.prototype.capitalize = function () {
    return this.split('\. ').map(x => x.charAt(0).toUpperCase() + x.slice(1)).join('\. ') + '\n'
}
const capitalize = new Transform({
    transform(chunk, encoding, callback) {
        this.push(chunk.toString().capitalize())
        callback()
    }
})
reading.pipe(capitalize).pipe(writing)