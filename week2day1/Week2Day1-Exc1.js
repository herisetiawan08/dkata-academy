const glob = require('glob')
const finder = (arg2, arg3, arg4) => {
    folder = process.argv[2]
    format = process.argv[3]
    type = process.argv[4]
    howDeep = '/'
    arg2 == undefined ? null : folder = arg2
    arg3 == undefined ? null : format = arg3
    arg4 == undefined ? null : type = arg4

    if (type == 'all') {
        howDeep = '/**/'
    }
    get = glob.sync(folder + howDeep + "*.+(" + format + ")", {});
    return get
}

module.exports = { finder }
