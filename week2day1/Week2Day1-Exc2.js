const fs = require('fs')
const readline = require('readline')

String.prototype.capitalize = function () {
    return this.split('\. ').map(x => x.charAt(0).toUpperCase() + x.slice(1)).join('\. ') + '\n'
}

const output = fs.createWriteStream('./theFile-result.txt')
const readInterface = readline.createInterface({
    input: fs.createReadStream('./theFile.txt'),
    output: output
});

readInterface.on('line', function (line) {
    output.write(line.capitalize())
});


