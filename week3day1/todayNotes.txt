//How to show all database in mongoDB
$ show dbs
//or
$ show databases

//How to choose or select a database
$ use <database name>

//How to show all collection
$ show collections

//Show or view a collection
$ db.<collection name>.find()
//Use this command if you dont wanna see the <key> name, 0 for hide, 1 for show
.find({},{<key>: 0})
//Use this command if you want see only the value that contain something special
.find({<key>: <value>})
//Or the key that contain a value
.find({<key>: /.*value.*/})

//Show in pretty view
.pretty()

//Using Sort in mongoDB
.sort(<key>: 1)
//Use 1 for ascending or -1 for descending

//Insert an documents into collection
$ db.<collection name>.insert(
     {
          <key>: <value>
     }
)

//Adding a index to find faster
$ db.<collection name>.createIndex(<key> : 1)

//Getting and counting just only yellow and black pets
$ db.pets.aggregate([ 
     {
          $match: {
               $or: [
                         {
                              "colour":"yellow"
                         },
                         {
                              "colour":"black"
                         }
                    ]
          }
     },
     {
          $group : {
               _id: '$colour',
               colour: {
                    $sum: 1
               }
          }
     }
])


db.pets.aggregate([ 
     {
          $match: {
               $or: [
                         {
                              "colour":/yellow/
                         },
                         {
                              "colour":/black/
                         }
                    ]
          }
     },
     {
          $group : {
               _id: '$colour',
               avg_Age: {
                   $avg: '$age'
               }
          }
     }
])