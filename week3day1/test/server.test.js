const Lab = require('@hapi/lab');
const btoa = require('btoa')
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('. ./lib/server')
const auth = btoa("heri:secret")

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: '/api/pets'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            headers: {
                Authorization: `Basic ${btoa('admin:admin')}`
            },
            url: '/api/pets'
        });
        expect(res.result.message).to.equal('Bad username or password');
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/pets'
        });
        expect(res.result.message).to.equal('Missing authentication');
    });

    it('Getting random pet from Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: '/api/pets/3'
        });
        expect(res.result).to.equal([
            {
                id: 3,
                name: 'xena',
                breed: 'golden retriever',
                colour: 'yellow',
                age: 4.5,
                next_checkup: '2020-2-6',
                vaccinations: ['Distemper shot', 'Kennel Cough', 'Dog Flu', 'Rabies']
            }
        ]);
    });


    it('What if i type alphabet in id', async () => {
        const res = await server.inject({
            method: 'get',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: '/api/pets/bla'
        })
        expect(res.statusCode).to.equal(400)
    })

    it('Try to sort and slice Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: '/api/pets?sort=name&limit=2&offset=4'
        });
        expect((res.result).length).to.equal(2);
    });

    it('Trying to push pet to Pets API', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/pets',
            headers: {
                Authorization: `Basic ${auth}`
            },
            payload: {
                name: "spinx",
                breed: "persian",
                colour: "pink",
                age: 5,
                next_checkup: '1995-06-17'
            }
        });
        expect(res.result.message).to.equal('created');
    });

    it('Trying to delete some pet', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: '/api/pets/2'
        })
        const soldedpet = await server.inject({
            method: 'get',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: '/api/pets?filter=sold'
        })
        expect(res.result.message).to.equal('deleted success')
        expect((soldedpet.result).length).to.equal(1)
    })
});