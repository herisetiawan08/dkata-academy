const Hapi = require('@hapi/hapi')
const routes = require('../routes')
const log = require('../tools/loging')
const Inert = require('inert')
const Path = require('path')

const server = Hapi.server({
    host: '0.0.0.0',
    port: 3000,
    routes: {
        files: {
            relativeTo: Path.join(__dirname, 'public')
        }
    }
})

server.events.on('response', (request) => {
    log(request);
});

const start = async () => {
    await server.register(Inert)
    server.route(routes.concat({
        method: 'GET',
        path: '/',
        handler: async (request, h) => {
            return h.response({ message: 'success' }).code(200)
        }
    })
    );
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};

const init = async () => {
    await server.initialize();
    return server;
};

start()

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

exports.init = init
exports.start = start