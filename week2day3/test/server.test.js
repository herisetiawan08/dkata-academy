'use strict'

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server');

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/'
        });
        expect(res.result.message).to.equal('success');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/profile/heri'
        });
        expect(`${res.result.first} ${res.result.last}`).to.equal('Heri Setiawan');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/profile/radha'
        });
        expect(`${res.result.first} ${res.result.last}`).to.equal('Radha Krishna');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/profile/naveen'
        });
        expect(`${res.result.first} ${res.result.last}`).to.equal('Naveen Ganapathy');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/heri'
        });
        expect(res.result).to.equal('Hi Heri');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/shruthi'
        });
        expect(res.result).to.equal('Hi Shruthi');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/radha'
        });
        expect(res.result).to.equal('Hi Radha');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/naveen'
        });
        expect(res.result).to.equal('Hi Naveen');
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/api/parsetime?iso=2019-11-27T03:01:56Z'
        });
        expect(res.payload).to.equal('{"hour":10,"minute":1,"second":56}');
    });
});