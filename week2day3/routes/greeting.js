const handler = require('../handler/handlerHapi')
const regex = require('../tools/regex')

module.exports = [{
    method: 'GET',
    path: '/{name}',
    handler: handler.greeting,
    options: regex.username
}]