const regex = require('../tools/regex')

module.exports = [{
    method: 'GET',
    path: '/images/{file*}',
    handler: {
        directory: {
            path: '../../image/',
            listing: true
        }
    }
}]