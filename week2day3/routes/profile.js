const handler = require('../handler/handlerHapi')
const regex = require('../tools/regex')

module.exports = [{
    method: 'GET',
    path: '/profile/{name}',
    handler: handler.profile,
    options: regex.username
}]