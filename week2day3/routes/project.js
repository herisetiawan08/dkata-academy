const regex = require('../tools/regex')

module.exports = [{
    method: 'GET',
    path: '/project/{file*}',
    handler: {
        directory: {
            path: '../../',
            listing: true
        }
    }
}]