const person = require('../data/listPerson')

exports.greeting = async (request, h) => {
    return h.response(`Hi ${request.params.name.charAt(0).toUpperCase() + request.params.name.slice(1)}`).code(200)
}
exports.profile = async (request, h) => {
    if (person[request.params.name] != undefined) {
        return h.response(person[request.params.name]).code(200)
    } else {
        return h.response({code: 404, message: "Page not found"}).code(404)
    }
}
exports.parsetime = async (request, h) => {
    var date = new Date(request.query.iso)
    var output = {
        hour: date.getHours(),
        minute: date.getMinutes(),
        second: date.getSeconds()
    }
    return h.response(output).code(200)
}
exports.home = async (request, h) => {
    return h.response({mesage: 'Success'}).code(200)
}