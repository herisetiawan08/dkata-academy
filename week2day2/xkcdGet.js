const axios = require('axios')
const request = require('request')
const fs = require('fs')
const path = require('path')
link = 'http://xkcd.com/info.0.json'
const downloader = (uri) => {
    imgSave = (url) => fs.createWriteStream('allComics/' + path.basename(url))
    request(uri)
        .pipe(imgSave(uri))
};
const preComic = async () => {
    try {
        const comic = await axios.get(link)
        const { img } = await comic.data
        downloader(img);
    } catch (err) {
        console.error(err)
    }
}
preComic()