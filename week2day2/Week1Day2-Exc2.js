const http = require('http')
const url = require('url')
var server = http.createServer(function (req, res) {
    var parUrl = url.parse(req.url, true)
    var path = parUrl.pathname
    var date = new Date(parUrl.query.iso)
    if (path === '/api/parsetime') {
        var output = {
            hour: date.getHours(),
            minute: date.getMinutes(),
            second: date.getSeconds()
        };
    }
    if (output) {
        res.writeHead(200, { 'content-type': 'application/json' })
        res.end(JSON.stringify(output))
    } else {
        res.writeHead(404, { 'message': 'Page not found' })
    }
});
server.listen(3000, console.log('Server running succesfully'))