const mongoose = require('mongoose')
const { Schema } = mongoose

const taskModel = new Schema({
    id: Number,
    title: String,
    description: String,
    createdAt: Date,
    updatedAt: Date,
    finishedAt: Date,
    YYYYMMDD: Number,
    status: String,
    comments: [
        {
            body: String,
            date: Date
        }
    ]
})

const Task = mongoose.model('task', taskModel)

module.exports = { Task }