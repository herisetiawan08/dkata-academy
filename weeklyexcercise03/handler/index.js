const { rootIndex } = require('./rootIndex')
const taskHandler = require('./taskHandler')

module.exports = {
    rootIndex,
    taskHandler
}