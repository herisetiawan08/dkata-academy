const rootIndex = async (request, h) => {
    return h.file('rootIndex.html')
}

module.exports = {
    rootIndex
}