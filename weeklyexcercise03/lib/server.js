const Path = require('path')
const Hapi = require('@hapi/hapi')
const Qs = require('qs')
const { mongodb } = require('./mongodb')
const routes = require('../routes')

const server = Hapi.server({
    host: "0.0.0.0",
    port: 3000,
    query: {
        parser: (query) => Qs.parse(query)
    },
    routes: {
        files: {
            relativeTo: Path.join(__dirname, './pages/')
        }
    }
})

const swaggerOptions = {
    info: {
        title: 'Task API Documentation',
        version: '0.0.1',
    }
};


const plugReg = async () => {
    await server.register([
        require('@hapi/inert'),
        require('@hapi/vision')
    ])
    server.route(routes)
    mongodb()
}


const start = async () => {
    try {
        await plugReg()
        await server.start()
        console.log(`Server running at: ${server.info.uri}`)
        return server
    } catch (err) {
        console.error(err.message)
        process.exit(1)
    }
}


const init = async() => {
    await server.initialize()
    return server
}
module.exports = {
    start,
    init,
    plugReg
}