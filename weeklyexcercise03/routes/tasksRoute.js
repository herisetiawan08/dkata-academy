const Joi = require('@hapi/joi')
const { taskHandler } = require('../handler')

const getTask = [
    {
        method: 'GET',
        path: '/tasks',
        handler: taskHandler.getTask,
        options: {
            description: 'Root path',
            notes: 'Greeting and welcoming users',
            tags: ['root'],
            validate: {
                query: {
                    filter: Joi.string().valid('new', 'in-progress', 'complete'),
                    sort: Joi.string().valid('id','title','createdAt','updatedAt','finishedAt','status'),
                    desc: Joi.boolean(),
                    date: Joi.date()
                }
            }
        }
    }
]

const getTaskById = [
    {
        method: 'GET',
        path: '/tasks/{id}',
        handler: taskHandler.getTaskById,
        options: {
            validate: {
                params: {
                    id: Joi.number().positive().integer()
                }
            }
        }
    }
]

const postTask = [
    {
        method: 'POST',
        path: '/tasks',
        handler: taskHandler.postTask,
        options: {
            validate: {
                payload: {
                    title: Joi.string().required(),
                    description: Joi.string().required(),
                    finishedAt: Joi.date().required()
                }
            }
        }
    }
]

const putTask = [
    {
        method: 'PUT',
        path: '/tasks/{id}',
        handler: taskHandler.putTask,
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().required()
                },
                payload: {
                    title: Joi.string(),
                    description: Joi.string(),
                    finishedAt: Joi.date(),
                    comments: Joi.string(),
                    status: Joi.string().valid('in-progress', 'complete')
                }
            }
        }
    }
]

const removeTask = [
    {
        method: 'DELETE',
        path: '/tasks/{id}',
        handler: taskHandler.removeTask,
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().required()
                }
            }
        }
    }
]

module.exports = {
    getTask,
    getTaskById,
    postTask,
    putTask,
    removeTask
}