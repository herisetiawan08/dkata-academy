const math = require('./SecondDay-exc2')
arr = ['ant', 'bird', 'elephant', 'penguin', 'fish', 'butterfly']
cities = ['jakarta', 'bandung', 'bogor']

test('Testing reducer', () => {
    expect(math.reducer(cities, 7)).toBe(14)
})

test('Testing counting length of each data of array', () => {
    expect(math.eachLen(arr)).toStrictEqual([3, 4, 8, 7, 4, 9])
})