const exp = (a,b) => {
    return a**b
}

const cube = (a) => {
    return exp(a,3)
}

const sqr = (a) => {
    return exp(a,2)
}

module.exports = {
    exp,
    cube,
    sqr
}