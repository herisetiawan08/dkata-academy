const sum = (a, b) => {
  return a + b
}

const reducer = (array, minlen) => {
  return array.map(a => a.length).filter(a => a >= minlen).reduce(sum)
}

const eachLen = (array) => {
  return array.map(a => a.length)
}

module.exports = { reducer, eachLen }