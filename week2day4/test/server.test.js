const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server')

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/pets'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('Getting random pet from Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/pets/3'
        });
        expect(res.result).to.equal([
            {
              id: 3,
              name: 'xena',
              breed: 'golden retriever',
              colour: 'yellow',
              age: 4.5,
              next_checkup: '2020-2-6',
              vaccinations: [ 'Distemper shot', 'Kennel Cough', 'Dog Flu', 'Rabies' ]
            }
          ]);
    });
    
    it('Try to sort and slice Pets API', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/pets?sort=name&limit=2&offset=4'
        });
        expect((res.result).length).to.equal(2);
    });

    it('Trying to push pet to Pets API', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/pets/push',
            payload: {
                name: "spinx",
                breed: "persian",
                colour: "pink",
                age: 5,
                next_checkup: 1995-06-17
            }
        });
        expect(res.result.message).to.equal('success');
    });
});