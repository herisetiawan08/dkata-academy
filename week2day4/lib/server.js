const Hapi = require('@hapi/hapi')
const api = require('../routes/api')
const log = require('../tools/log')

const server = Hapi.server({
    host: '0.0.0.0',
    port: 3000,
});

server.route(api);
const start = async () => {
    await server.start();
    console.log('Server running at:', server.info.uri);
    server.events.on('response', (request) => {
        log(request);
    });
    return server
};

init = async () => {
    await server.initialize();
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

exports.start = start
exports.init = init