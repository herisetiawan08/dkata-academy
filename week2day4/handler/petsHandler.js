const pets = require('../data/myPet.json')

exports.pets = async (request, h) => {
    let { sort, limit, offset } = request.query
    if (sort, limit, offset != undefined) {
        let parseLim = parseInt(limit)
        let parseOff = parseInt(offset)
        let petsort = pets
            .sort((a, b) => (a[sort] > b[sort]) ? 1 : -1)
            .slice(parseOff, parseLim + parseOff)
        return h.response(petsort)
    }
    return h.response(pets);
}

exports.petsId = async (request, h) => {
    return h.response(pets.filter(x => x.id == request.params.id))
}

exports.setPet = async (request, h) => {
    let curId = (Math.max(...pets.map(x => x.id)))
    request.payload.id = ++curId
    pets.push(request.payload)

    return h.response({ message: 'created' }).code(201)
}

exports.putPet = async (request, h) => {
    if (pets.find(x => x.id == request.params.id) != undefined) {
        Object.assign(pets.find(x => x.id == request.params.id), { next_checkup: request.payload.next_checkup })
        return h.response({ message: 'updated' }).code(202)
    } else {
        return h.response({ message: 'not found' }).code(404)
    }
}