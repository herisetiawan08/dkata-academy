const Joi = require('@hapi/joi')

exports.num = Joi.number().integer()
exports.query = {
    lowercase: Joi.string().regex(/^[a-z]$/),
    num: Joi.string().regex(/^[0-9]{1,2}$/)
    
}