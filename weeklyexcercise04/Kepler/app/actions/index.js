import axios from 'axios'

const getTasksSuccess = (data) => {
    return {
        type: 'GET_TASKS_SUCCESS',
        payload: data
    }
}

const getTasksFailed = (err) => {
    return {
        type: 'GET_TASKS_FAILED',
        payload: {
            error: err
        }
    }
}

export const getAllTask = (date) => {
    let url = `http://192.168.43.245:3000/tasks?sort=finishedAt`
    if (date) {
        url = `http://192.168.43.245:3000/tasks?date=${date}`
    }
    return dispatch => {
        axios
            .get(url)
            .then(response => dispatch(getTasksSuccess(response.data)))
            .catch(err => dispatch(getTasksFailed(err)))
    }
}

export const getTaskByDate = (date) => {
    return dispatch => {
        axios
            .get(`http://192.168.43.245:3000/tasks?date=${date}`)
            .then(response => dispatch(getTasksSuccess(response.data)))
            .catch(err => dispatch(getTasksFailed(err)))
    }
}

export const updateTask = (id, title, desc, finishedAt) => {
    return dispatch => {
        axios
            .put(`http://192.168.43.245:3000/tasks/${id}`, {
                title: title,
                description: desc,
                finishedAt: finishedAt
            })
            .then(() => dispatch(getAllTask()))
            .catch(err => dispatch(getTasksFailed(err)))
    }
}

export const createTask = (title, desc, finishedAt) => {
    return dispatch => {
        axios
            .post(`http://192.168.43.245:3000/tasks`, {
                title: title,
                description: desc,
                finishedAt: finishedAt
            })
            .then(() => dispatch(getAllTask()))
            .catch(err => dispatch(getTasksFailed(err)))
    }
}


export const addComment = (id, comment) => {
    return dispatch => {
        axios
            .put(`http://192.168.43.245:3000/tasks/${id}`, {
                comments: comment
            })
            .then(() => dispatch(getAllTask()))
            .catch(err => dispatch(getTasksFailed(err)))
    }
}

export const markAs = (id, status) => {
    return dispatch => {
        axios
            .put(`http://192.168.43.245:3000/tasks/${id}`, {
                status: status
            })
            .then(() => dispatch(getAllTask()))
            .catch(err => dispatch(getTasksFailed(err)))
    
    }
}