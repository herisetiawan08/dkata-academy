import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { style } from '../styles'

const renderTasks = (task, navigate, finder) => {
    const textHandler = (string, maxlen) => {
        return string.slice(0, maxlen) + (string.length > maxlen ? '...' : '')
    }
    if (((task.title).toLowerCase()).includes(finder.toLowerCase()) == false) {
        return null
    }
    return (
        <TouchableOpacity onPress={() => navigate('Task', {task: task})}>
            <View style={style.eachContainer}>
                <View style={style.titleDate}>
                    <Text style={style.taskTitleText}>
                        {textHandler(task.title, 33)}
                    </Text>
                    <Text style={style.taskDate}>
                        {(task.finishedAt).slice(0, 10).replace('-', '/').replace('-', '/')}
                    </Text>
                </View>
                <Text style={style.taskDescText}  numberOfLines={1}>
                    {textHandler(task.description, 50)}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default renderTasks