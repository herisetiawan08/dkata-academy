import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { style } from '../styles'

const renderComments = (comment) => {
    return (
        <View style={[style.commentList, style.titleDate]}>
            <Text style={style.commentBody}>
                {comment.body}
            </Text>
            <Text style={style.commentDate}>
                {comment.date.slice(0, 10).replace('-', '/').replace('-', '/')}
            </Text>
        </View>
    )
}

export default renderComments