import React from 'react'
import { DatePickerAndroid } from 'react-native'

const setDateAndroid = async () => {
    try {
        const {
            action, year, month, day
        } = await DatePickerAndroid.open({
            date: new Date(),
            minDate: new Date()
        })
        if (action !== DatePickerAndroid.dismissedAction) {
            return `${year}-${month + 1}-${day}`
        }
    } catch ({ code, message }) {
        console.warn('Can\'t open date picker', message)
    }
}

export { setDateAndroid }