import React, { Component } from 'react'
import { TouchableOpacity, TextInput, ActivityIndicator, View, Text, StatusBar, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { getAllTask } from '../actions'
import { style, mainColor, secColor } from '../styles'
import renderTasks from './TaskList'
import DateTimePicker from '@react-native-community/datetimepicker'
import Icon from 'react-native-vector-icons/AntDesign'


class MainPage extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = ({
            dateISO: new Date().toISOString().slice(0, 10),
            mode: 'date',
            datePicker: false,
            finder: '',
            emptyDate: true,
            search: false
        })
    }

    componentDidMount() {
        this.resetDate()
    }

    resetDate = () => {
        this.props.getAllTask()
        this.setState({
            emptyDate: true
        })
    }

    setDate = (event, date) => {
        date = date || this.state.date
        this.setState({
            datePicker: false,
            emptyDate: false,
            dateISO: date.toISOString().slice(0, 10)
        })
        this.props.getAllTask(this.state.dateISO)
    }

    render() {
        if (this.props.task.loading) {
            return (
                <View style={[style.container, style.fullContainer]}>
                    <ActivityIndicator color={secColor} />
                </View>
            )
        }
        return (
            <View style={[style.container, style.fullContainer]}>
                <View style={style.headerContainer}>
                    <TouchableOpacity onPress={() => this.setState({ search: !this.state.search })}>
                        <Icon name='search1' color={secColor} size={30} />
                    </TouchableOpacity>
                    <Text style={style.txtTitle}>
                        MY TASKS
                    </Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Task', { task: { status: 'add' } })}>
                        <Icon name='pluscircleo' color={secColor} size={30} />
                    </TouchableOpacity>
                </View>
                {this.state.search &&
                    <>
                        <TextInput
                            style={style.findForm}
                            placeholder={"Find title"}
                            placeholderTextColor={mainColor}
                            onChangeText={(finder) => this.setState({ finder })}
                            returnKeyType={'search'}
                        />
                        <View style={style.datePickerContainer}>
                            <View style={[style.datePicker, style.datePickerSeparator]}>
                                <TouchableOpacity onPress={() => this.setState({ datePicker: !this.state.datePicker })}>
                                    <Text style={style.datePickerTxt}>
                                        {this.state.emptyDate ? "Find by Date" : this.state.dateISO}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={style.datePicker}>
                                <TouchableOpacity onPress={this.resetDate}>
                                    <Text style={style.datePickerTxt}>Reset</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </>
                }
                {this.state.datePicker &&
                    <DateTimePicker
                        value={Date.parse(this.state.dateISO)}
                        mode='date'
                        display='default'
                        onChange={this.setDate}
                    />
                }
                <FlatList
                    style={[style.fullContainer, style.taskListContainer]}
                    keyExtractor={(item, index) => index.toString()}
                    data={(this.props.task.tasks)}
                    renderItem={(task) => renderTasks(task.item, this.props.navigation.navigate, this.state.finder)}
                />
            </View >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        task: state.task
    }
}

export default connect(mapStateToProps, { getAllTask })(MainPage)