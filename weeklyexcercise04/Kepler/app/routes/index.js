import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import MainPage from '../components/MainPage'
import TaskEditor from '../components/TaskEditor'

const rootStack = createStackNavigator(
    {
        Home: { screen: MainPage },
        Task: { screen: TaskEditor }
    },
    {
        initialRouteName: 'Home'
    }
)

const Container = createAppContainer(rootStack)

export default Container