const initialState = {
    loading: true,
    tasks: [],
    error: null
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_TASKS_SUCCESS':
            return {
                ...state,
                loading: false,
                tasks: action.payload
            }
        case 'GET_TASKS_FAILED':
            return {
                ...state,
                error: action.payload.error
            }
        default:
            return state
    }
}

export { taskReducer }