import { combineReducers } from 'redux'
import { taskReducer } from './taskReducer'

const combinedReducers = combineReducers({
    task: taskReducer
})

const rootReducers = (state, action) => {
    return combinedReducers(state, action)
}

export default rootReducers