import React, { Component } from 'react'
import { StatusBar } from 'react-native'
import { Provider } from 'react-redux'
import Container from './routes'
import store from './store'
import { mainColor } from './styles'

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <StatusBar backgroundColor={mainColor} />
                <Container />
            </Provider>
        )
    }
}