//Step here is useless, try step in bottom in this file
const person = () => {
    name = ''
    birth = ''
    address = ''
    gender = ''
    const setPerson = (name, birth, address, gender) => {
        name = name
        birth = birth
        address = address
        gender = gender
    }
    const getPerson = () => {
        return {
            name: name,
            birth: birth,
            address: address,
            gender: gender
        }
    }
    return { setPerson, getPerson }
}

const trainer = () => {
    const thisTrainer = person
    const { name, birth, address, gender } = thisTrainer().getPerson()
    skill = ''
    const setPerson = (name, birth, address, gender, skill) => {
        thisTrainer().setPerson(name, birth, address, gender)
        skill = skill
    }
    const getPerson = () => {
        return {
            name: name,
            birth: birth,
            address: address,
            skill: skill
        }
    }
    return { setPerson, getPerson }
}

const trainee = () => {
    const thisTrainee = person
    const {name,birth,address,gender} = thisTrainee().getPerson()
    study = ''
    const setPerson = (name, birth, address, gender, study ) => {
        thisTrainee().setPerson(name,birth,address,gender)
        study = study
    }
    const getPerson = () => {
        return {
            name: name,
            birth: birth,
            address: address,
            gender: gender,
            study: study
        }
    }
}

module.exports = {
    trainer: trainer,
    trainee: trainee
}

