const mongoose = require('mongoose')
const { Schema } = mongoose

const petModel = new Schema({
    id: Number,
    name: String,
    breed: String,
    colour: String,
    age: Number,
    next_checkup: Date,
    vaccinations: Array,
    sold: Boolean,
    image: String

})
const Pet = mongoose.model('pet', petModel)

module.exports = { Pet }
