const Joi = require('@hapi/joi')

exports.num = Joi.number().integer()
exports.query = {
    lowercase: Joi.string().regex(/^[a-z]$/),
    num: Joi.string().regex(/^[0-9]{1,2}$/)
    
}
exports.petReq = {
    name: Joi.string().regex(/^[A-Za-z\s]{3,}$/),
    breed: Joi.string().regex(/^[A-Za-z\s]{3,}$/),
    colour: Joi.string().regex(/^[A-Za-z\s\/]{3,}$/),
    age: Joi.number().integer(),
    next_checkup: Joi.date(),
    vaccinations: Joi.array()
}
