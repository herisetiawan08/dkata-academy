const mongoose = require('mongoose')
const mongoUrl = 'mongodb://localhost:27017/dkata-academy'

const connectDB = async () => {
    try {
        await mongoose.connect(mongoUrl, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });
        console.log(`MongoDB connect at: ${mongoUrl}`)
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};

module.exports = {connectDB}