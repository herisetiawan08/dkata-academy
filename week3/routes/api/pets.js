const Joi = require('@hapi/joi')
const handler = require('../../handler/petsHandler')
const valid = require('../../validator')

exports.greeting = [
    {
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return h.file('index.html')
        }
    }
]

exports.upPetImg = [
    {
        method: 'POST',
        path: '/image/{id}',
        config: {
            payload: {
                output: "stream",
                parse: true,
                maxBytes: 2 * 1000 * 1000
            }
        },
        handler: handler.upPetImg
    }
]

exports.petImage = [
    {
        method: 'GET',
        path: '/image/{picname}',
        handler: (request, h) => {
            return h.file(`img/${request.params.picname}`)
        },
        options: {
            auth: false
        }
    }
]

exports.pets = [
    {
        method: 'GET',
        path: '/api/pets',
        options: {
            description: 'Get pets list',
            notes: 'Returns an array of your pets',
            tags: ['api', 'pets'],
            validate: {
                query: {
                    sort: Joi.string().valid('id', 'name', 'breed', 'colour', 'age', 'next_checkup', 'vaccinations'),
                    limit: Joi.number().integer(),
                    offset: Joi.number().integer(),
                    filter: {
                        sold: Joi.boolean()
                    }
                }
            }
        },
        handler: handler.pets,
    }
]

exports.petsId = [
    {
        method: 'GET',
        path: '/api/pets/{id}',
        handler: handler.petsId,
        options: {
            validate: {
                params: {
                    id: valid.num
                }
            }
        }
    }
]

exports.setPet = [
    {
        method: 'POST',
        path: '/api/pets',
        handler: handler.setPet,
        options: {
            validate: {
                payload: valid.petReq
            }
        }
    }
]

exports.putPet = [
    {
        method: 'PUT',
        path: '/api/pets/{id}',
        handler: handler.putPet,
        options: {
            validate: {
                payload: valid.petReq,
                params: {
                    id: valid.num
                }
            }
        }
    }
]

exports.delPet = [
    {
        method: 'DELETE',
        path: '/api/pets/{id}',
        handler: handler.delPet,
        options: {
            validate: {
                params: {
                    id: valid.num
                }
            }
        }
    }
]

exports.allPet = [
    {
        method: 'GET',
        path: '/api/pets/all',
        handler: handler.allPet
    }
]
